package com.example.demo.test.springjpa;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "[user]")
@Data
public class User implements Serializable {
    private static final long serialVersionUID = -297553281792804396L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //mapping variables with columns in database
    @Column(name = "hp")
    private int hp;
    @Column(name = "stamina")
    private int stamina;

    //  without danh dau se tu dong mapping theo ten bien
    private int atk;
    private int def;
    private int agi;

}
