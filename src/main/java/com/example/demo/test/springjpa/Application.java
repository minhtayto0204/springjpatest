package com.example.demo.test.springjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.List;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Application.class, args);
		UserRepository userRepository = context.getBean(UserRepository.class);

		// get all the user from db
		userRepository.findAll().forEach(System.out::println);
		System.out.println("=============\n");
		// get user from db
		User user2 = userRepository.findById(6L).get();
		System.out.println("User: "+user2);
		// count number of user in list
		int num = userRepository.findAll().size();
		System.out.println("Number of user: "+num);
		// update for user
		user2.setAtk(100);
		userRepository.save(user2);
		System.out.println("user2: "+user2);



//		// save new user to db
//		User user = userRepository.save(new User());
//		// when done saving user, return user with id
//		System.out.println("ID of new user : "+user.getId());
//
//		Long userId = user.getId();
//
//		// update for new user
//		user.setAtk(6969);
//		userRepository.save(user); // save information just updated
//
//		// get user from table
//		User user2 = userRepository.findById(userId).get();
//		System.out.println("User: "+user);
//		System.out.println("User2: "+user2);
//
//		// detele user from db
//		userRepository.delete(user);
//
//		// check deleted user still on the db or not ?
//		User user3 = userRepository.findById(userId).orElse(null);
//		System.out.println("User 3: "+user);

	}

}
