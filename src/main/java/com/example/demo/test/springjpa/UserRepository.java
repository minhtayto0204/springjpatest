package com.example.demo.test.springjpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository // handle the comunicate with DB
public interface UserRepository extends JpaRepository<User, Long> {
    // context will find and create userRepository instance
    // viec tao ra la hoan toan tu dong
}
